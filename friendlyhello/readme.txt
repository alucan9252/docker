#Construir imagen de la aplicación

docker build -t ttania/friendlyhello .

#Arrancar el contenedor

docker run --rm -p 4000:80 friendlyhello

#Arrancar el docker compose

docker-compose up -d --scale web=5
